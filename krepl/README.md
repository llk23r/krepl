## KREPL - REPL for an in-memory K/V store

Install poetry from https://python-poetry.org/docs/#installation
Run `poetry shell` to activate the virtual environment.

Optional: Run `poetry install` to install the dependencies listed in `pyproject.toml`. Only needed if the `.venv` directory does not exist or gets deleted.
Optional: Run `pre-commit install` to install the pre-commit hooks.


To launch the repl, execute `backend-python/krepl/krepl/launch.py`. In case of import error, ensure that python path is set by running `export PYTHONPATH="${PYTHONPATH}:${PWD}"`

Before launching the repl, view the repl config at `/backend-python/krepl/krepl/config.py`
    - `MAX_INPUT_SIZE` - It determines the maximum size of the string calculated by `getsizeof()`. It's meant to tackle a case where the input for write might be greater than the available memory itself.
    - `LOG_TRANSACTION` - It's a boolean that toggles if the input commands should be written. If enabled it prints the list of commands executed so far and writes it to a file.
    - `LOG_PATH` - Specifies where the LOG_TRANSACTION output should be. It's an emulation of a WAL, which can be used for debugging or for recovery(will need more extensive implementation for recovery via replaying the logs).


Design:
    - File Descriptions:
        - krepl/krepl/db/database.py - Has a Database class with `root` that is a dictionary to hold the key, value pairs.

        - krepl/krepl/db/transactions.py - Defines a decorator `@log_transaction` which is enabled when the `LOG_TRANSACTION` is set to True in config.py.
                                        - It also has `TransactionBlock` which has methods available to interact with the Database. It inherits Database.

        - krepl/krepl/repl/dispatcher.py - It defines a mapping for the input command and the handler function for that command. It also has mapping for valid argument count for each command. Since `valid_arg_count` for `WRITE` is set to 2, it means a key cannot have space-separated/multi-worded values.

        - krepl/krepl/repl/input_manager/validator.py - It has a method that validates the incoming input by checking for various cases. It also has a method to call the dispatcher if validation succeeds.

        - krepl/krepl/repl/input_manager/printer.py - It either prints to the stdout or raises error which later gets printed to stderr.

        - krepl/krepl/repl/constants.py - It has constants defined in once place. So, if the commands were to change the way they were named or the error message thrown when key is not found or when there is no transaction, it needs to be changed only in one place here.

        - krepl/krepl/launch.py - This is the file to run to start the repl.


    - Decisions and tradeoffs:
        - The solution is optimized for read with read time complexity of O(1) as it uses a dictionary as the key-value database.
        - The solution uses Deque - It's a stack-implementation using Doubly LinkedList. So, the operations for insertions are faster too.
        - Usage of slots - It uses `dataclass(slots=True)` to ensure better performance when dealing with objects.
        - The solution has a feature to write command logs for debugging and recovery management. It is implemented by implementing a decorator which is cached using the lru cache.
        - For writes - it first reads, compares the existing-value with incoming-value for the key; if existing-value == incoming-value, it returns True. If they're not the same, it then checks if the incoming key is present in top of the stack (current-transaction). If not, it copies over the existing db value for the key to the current transaction. It then updates the db. It is not usually how dbs work but this was necessary to handle the case where a COMMIT doesn't close the transactions before it but only closes the current transaction.
        - For commit - It removes the current transaction dictionary from the transactions since only the current transaction has to be "completed" and the changes present in the transaction is already present in the db because of the way WRITE works. It throws an error if transaction is not present.
        - For abort - It either deletes the key from the db or copies data from transaction to the db. It throws an error if transaction is not present.

    - Tests:
        - Tests are written in `krepl/krepl/test_krepl.py` and it uses pytest.
        - Coverage of the test is as below:

                    ---------- coverage: platform darwin, python 3.10.2-final-0 ----------
                    Name                              Stmts   Miss  Cover
                    -----------------------------------------------------
                    __init__.py                           1      0   100%
                    config.py                             3      0   100%
                    db/__init__.py                        0      0   100%
                    db/database.py                       17      4    76%
                    db/transactions.py                   80     16    80%
                    launch.py                            15      5    67%
                    repl/__init__.py                      0      0   100%
                    repl/constants.py                    25      0   100%
                    repl/dispatcher.py                    8      0   100%
                    repl/input_manager/__init__.py        0      0   100%
                    repl/input_manager/printer.py         8      0   100%
                    repl/input_manager/processor.py      24      6    75%
                    repl/input_manager/validator.py      39      7    82%
                    test_krepl.py                        95      0   100%
                    -----------------------------------------------------
                    TOTAL                               315     38    88%


                    =============================================================================== 3 passed in 0.04s ================================================================================

    - Code Standards:
        - It uses black, flake8, mypy, isort and interrogate to ensure consistent coding standard.
    