from typing import Optional

from krepl.krepl.db.transactions import TransactionBlock
from krepl.krepl.repl.input_manager.processor import process_input


class Shell:
    """Shell class that will be used to interact with the transactions which then interact with the database"""

    def __init__(self, txn: TransactionBlock):
        """Initialize the Shell class with a TransactionBlock object called root_txn"""
        self.root_txn = txn

    def repl(self, test_input_str: Optional[str] = "") -> Optional[bool] | str:
        """Process input and return outcome.
        If test_input_str is passed, it runs the test input string.
        Otherwise, it runs the input from the user.
        """
        if test_input_str:
            return process_input(self.root_txn, test_input_str)
        else:
            while True:
                process_input(self.root_txn)


if __name__ == "__main__":
    """This is the main function that will be used to interact with the k/v repl"""
    blk = TransactionBlock()
    shell = Shell(blk)
    shell.repl()
