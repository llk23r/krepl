from typing import Optional

import pytest

from krepl.krepl.db.transactions import TransactionBlock
from krepl.krepl.launch import Shell
from krepl.krepl.repl.constants import Command, InactiveTransactionOutcome, ReadOutcome


class TestDatabase:
    """This is the test suite for the krepl program"""

    def _test_read(
        self,
        key: str,
        expected: Optional[str] = "",
        error: Optional[bool] = False,
    ) -> bool:
        """
        Test read command
        - Should read the value of the key from the last dictionary in the transaction if transaction is active
        - Should throw an error if the key is not found
        """
        read_input = f"{Command.READ.value} {key}"
        read_output = shell.repl(read_input)
        if error:
            assert read_output.message == f"{ReadOutcome.ERROR.value}: {key}"
        else:
            assert read_output == expected
        return True

    def _test_write(
        self, key: str, value: str, expected: Optional[bool] = True
    ) -> bool:
        """
        Test write command
        - Should store the key, value in the last dictionary in the transaction if transaction is active
        - Should store the key, value in the root dictionary if transaction is not active
        """
        assert shell.repl(f"{Command.WRITE.value} {key} {value}") == expected
        return True

    def _test_delete(self, key: str, expected: Optional[bool] = True) -> bool:
        """
        Test delete command
        - Should throw an error if no transaction is active
        - Should delete the last dictionary in the transaction
        """
        assert shell.repl(f"{Command.DELETE.value} {key}") == expected
        return True

    def _test_start(self) -> bool:
        """
        Test start command
        - Should create add a new dictionary to the end of the transaction
        """
        assert shell.repl(f"{Command.START.value}") is True
        return True

    def _test_commit(
        self, error: Optional[bool] = False, expected: Optional[bool] = True
    ) -> bool:
        """
        Test commit command
        - Should throw an error if no transaction is active
        - Should commit the transaction, ie. merge the last dictionary in the transaction with the root dictionary
        """
        commit_error = f"{InactiveTransactionOutcome.ERROR.value}"
        commit_input = shell.repl(f"{Command.COMMIT.value}")
        if error:
            assert commit_input == commit_error
        else:
            assert commit_input == expected
        return True

    def _test_abort(
        self, error: Optional[bool] = False, expected: Optional[bool] = True
    ) -> bool:
        """
        Test abort command
        - Should throw an error if no transaction is active
        - Should delete the last dictionary in the transaction
        """
        commit_input = shell.repl(f"{Command.ABORT.value}")
        commit_error = f"{InactiveTransactionOutcome.ERROR.value}"
        if error:
            assert commit_input == commit_error
        else:
            assert commit_input == expected
        return True

    def _test_quit(
        self,
    ) -> None:
        """
        Test quit command
        - Should throw an error if no transaction is active
        - Should quit the program
        """
        with pytest.raises(SystemExit) as q_ex:
            shell.repl(f"{Command.QUIT.value}")
        assert q_ex.value.code == 0

    def test_example_run(self):
        """
        Test example run
        - Should run the example program
        """
        self._test_write("a", "hello")
        self._test_read("a", "hello")
        self._test_start()
        self._test_write("a", "hello-again")
        self._test_read("a", "hello-again")
        self._test_start()
        self._test_delete("a")
        self._test_read("a", error=True)
        self._test_commit()
        self._test_read("a", error=True)
        self._test_write("a", "once-more")
        self._test_read("a", "once-more")
        self._test_abort()
        self._test_read("a", "hello")
        self._test_commit(error=True)
        self._test_abort(error=True)
        self._test_quit()

    def test_nested_transactions(self):
        """
        Test nested transactions
        """
        self._test_start()
        self._test_write("a", "10")
        self._test_start()
        self._test_write("a", "20")
        self._test_start()
        self._test_write("a", "30")
        self._test_commit()
        self._test_read("a", "30")
        self._test_abort()
        self._test_read("a", "10")
        self._test_abort()
        self._test_read("a", error=True)
        self._test_quit()

    def test_nested_transactions_2(self):
        """
        Variant to test nested transactions
        """
        self._test_write("A", "30")
        self._test_start()
        self._test_write("A", "40")
        self._test_write("B", "50")
        self._test_write("C", "80")
        self._test_start()
        self._test_write("A", "100")
        self._test_write("B", "120")
        self._test_read("A", "100")
        self._test_read("B", "120")
        self._test_read("C", "80")
        self._test_abort()
        self._test_read("A", "40")
        self._test_read("B", "50")
        self._test_read("C", "80")
        self._test_abort()
        self._test_read("A", "30")
        self._test_read("B", error=True)
        self._test_read("C", error=True)
        self._test_quit()


# db = Database()
blk = TransactionBlock()
shell = Shell(blk)
