""" Contains logic for the repl db """
try:
    import sys
    from dataclasses import dataclass, field
    from typing import Optional

    from krepl.krepl.repl.constants import PythonPathNotSetOutcome
except ImportError as e:
    print(e, file=sys.stderr)
    print(PythonPathNotSetOutcome.INFO.value, file=sys.stderr)
    sys.exit(1)


@dataclass(slots=True)
class Database:
    """A database that stores key-value pairs. It is a dictionary stored in 'root'"""

    root: dict = field(default_factory=dict)

    def read(self, key: Optional[str]) -> Optional[str]:
        """Reads a value frlom the database."""
        return self.root.get(key, None)


class KeyNotFoundError(Exception):
    """Raised when a key is not found in the database."""

    def __init__(self, message: Optional[str]) -> None:
        """Initializes a KeyNotFoundError."""
        self.message = message if message else ""
