import sys
import warnings
from dataclasses import dataclass, field
from datetime import datetime
from functools import lru_cache
from typing import Deque, List, Optional

from krepl.krepl.config import LOG_PATH, LOG_TRANSACTION
from krepl.krepl.db.database import Database
from krepl.krepl.repl.constants import (
    ExitOutcome,
    InactiveTransactionOutcome,
    LogPathNotSetOutcome,
)


@lru_cache(maxsize=None)
def log_transaction(func):
    """Decorator that logs the transaction.
    It picks the log path from the config file.
    LOG_PATH: It logs the transaction in the log file under LOG_PATH.
    LOG_TRANSACTION: should be set to True in the config file for logging to be enabled.
    """
    if not LOG_TRANSACTION:
        return func

    def wrapper(*args, **kwargs):
        """Wrapper function that logs the transaction."""
        self = args[0]
        command, command_args = (
            f"{func.__name__.upper()}",
            f"{' '.join(args[1:])}",
        )
        message = f"{command} {command_args}" if command_args else command
        if LOG_PATH:
            with open(LOG_PATH, "a") as f:
                f.write(f"{datetime.now()} {message}\n")
        else:
            warnings.warn(LogPathNotSetOutcome.WARNING.value)
        self.append_transaction(message)
        print(self._Database__transaction_log)

        return func(*args, **kwargs)

    return wrapper


class TransactionBlock(Database):
    """A class that represents a transaction block."""

    def __init__(self) -> None:
        """Initializes a TransactionBlock.
        transactions - is a list of transactions where each transaction is a dictionary. It uses Deque instead of a list for stack implementation.
        transaction_log - is a list of commands that are logged. It emulates a WAL (Write-Ahead-Log) for the database. It is config driven and can be turned off by setting LOG_TRANSACTION to False.
        root - is a dictionary that stores the key-value pairs.
        """
        self.__transactions: Deque = Deque()
        self.__transaction_log: List[str] = []

        super().__init__()
        self.__root: dict = self.root

    def append_transaction(self, message):
        """Appends a transaction to the transaction log."""
        self.__transaction_log.append(message)

    def read(self, key: Optional[str]) -> Optional[str]:
        """Reads a value from the database. It calls the read method of the parent class - Database.
        It directly queries the db(dict) and does not use the transaction log. The time complexity is O(1).
        """
        return super().read(key)

    @log_transaction
    def write(self, key: str, value: str) -> bool:
        """Writes a value to the database.
        It first fetches the value from the database, compares it to the value to be written.
        If the values are different, it checks if the key is present in the current transaction.
        If the key is present, it updates the value in the transaction.
        It then writes the value to the database."""
        current_value = self.read(key)
        if current_value == value:
            return True
        if self.__transactions and key not in self.__transactions[-1]:
            self.__transactions[-1][key] = current_value

        self.__root[key] = value
        return True

    @log_transaction
    def delete(self, key: str) -> bool:
        """Deletes a key from the database(root) first. It then checks if the key is present in the current transaction.
        If the key is not present, it copies over the key, value from the dictionary to the current transaction.
        """
        current_value = self.__root.pop(key, None)
        if current_value is None:
            return True
        if self.__transactions and key not in self.__transactions[-1]:
            self.__transactions[-1][key] = current_value
        return True

    @log_transaction
    def start(self) -> bool:
        """Starts a transaction. It adds an empty dictionary to transactions and returns True."""
        self.__transactions += [{}]
        return True

    @log_transaction
    def commit(self) -> str | bool:
        """Commits a transaction.
        For the example run, where the commit only closes the commit in the inner-most scope or the current scope it only removes the last element from the transactions stack
        essentially removing the current transaction from the stack instead of actually committing it since the data is already written to the database.
        """
        if not self.__transactions:
            return InactiveTransactionOutcome.ERROR.value
        self.__transactions.pop()
        return True

    @log_transaction
    def abort(self) -> bool | str:
        """Aborts a transaction.
        If the transaction stack is empty, it returns an error message.
        If the transaction stack is not empty, it removes the last element from the transactions stack, then for the keys that are set to None, it removes the key from the root.
        If the key is a non-None value, it sets the value for the key in the root essentially updating the root with the value from the transaction.
        """
        if not self.__transactions:
            return InactiveTransactionOutcome.ERROR.value

        for key, value in self.__transactions.pop().items():
            current_value = self.read(key)
            if current_value == value:
                continue
            if value is None:
                del self.__root[key]
            else:
                self.__root[key] = value
        return True

    @log_transaction
    def quit(self) -> None:
        """Quits the program.
        It then clears the transaction log and the db - root dictionary.
        Exits the program with the appropriate exit code - 0 for success and 1 for failure."""
        print(ExitOutcome.MESSAGE.value)
        self.__transactions.clear()
        self.__root.clear()
        sys.exit(0)
