""" Contains the validation-related methods for the command processor. """
import sys
from typing import List, Optional, Tuple

from krepl.krepl.config import MAX_INPUT_SIZE
from krepl.krepl.db.database import Database, KeyNotFoundError
from krepl.krepl.db.transactions import TransactionBlock
from krepl.krepl.repl.constants import InvalidInputOutcome, LargeInputOutcome
from krepl.krepl.repl.dispatcher import construct_command_map


def validate_map(command_map: dict, command: str, args: List[str]) -> bool:
    """Validates the command and the arguments."""

    def is_command_valid(command: str, command_map: dict) -> bool:
        """Validates the command by checking if it is in the command_map."""
        return command in command_map

    def is_arg_count_valid(command: str, args: List[str], command_map: dict) -> bool:
        """Validates the argument count by checking if the argument count is valid."""
        valid_arg_count = command_map[command]["valid_arg_count"]
        return len(args) == valid_arg_count

    if not is_command_valid(command, command_map):
        raise ValueError(f"Invalid command: {command}")

    if not is_arg_count_valid(command, args, command_map):
        valid_arg_count = command_map[command]["valid_arg_count"]
        raise ValueError(
            f'{command} requires {valid_arg_count} argument{"s"[:valid_arg_count^1]} but {len(args)} were provided'
        )
    return True


def validate_and_dispatch_command(
    command: str, args: List[str], txn: TransactionBlock
) -> Optional[bool]:
    """Validates the command and the arguments and calls the dispatcher."""
    try:
        command_map = construct_command_map(args, txn)
        validate_map(command_map, command, args)
        return command_map[command][
            "dispatcher"
        ]()  # dispatch command to handler function in command_map dictionary
    except KeyNotFoundError as e:
        raise KeyNotFoundError(e)
    except Exception as e:
        raise RuntimeError(
            f"""Error while executing command:
                parsed command: {command}
                parsed arguments: {args}
                error: {e}
                """
        )


def parse_input(input_str: str) -> Tuple[str, List[str]]:
    """Parses the input string and returns the command and the arguments."""
    command, *kv_args = input_str.split()
    return command, kv_args


def _is_input_valid(input_str):
    """Validates the input string.
    It checks if the input string is empty, too long, not a printable-ascii, if the input size exceeds the limit set in the config."""
    if not input_str:
        return False
    if not (input_str.isascii() and input_str.isprintable()):
        raise ValueError(InvalidInputOutcome.ERROR.value)
    if sys.getsizeof(input_str) > MAX_INPUT_SIZE:
        raise ValueError(f"{LargeInputOutcome.ERROR.value}: {MAX_INPUT_SIZE}")
    return True
