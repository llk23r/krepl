""" Contains command processing logic """
import sys
from typing import Optional

from krepl.krepl.db.database import Database, KeyNotFoundError
from krepl.krepl.db.transactions import TransactionBlock
from krepl.krepl.repl.constants import ExitOutcome
from krepl.krepl.repl.input_manager.validator import (
    _is_input_valid,
    parse_input,
    validate_and_dispatch_command,
)


def process_input(
    txn: TransactionBlock, test_input_str: Optional[str] = ""
) -> Optional[bool] | str:
    """Process input and return outcome.
    It validates if the input is valid, parses the input and calls the dispatcher.
    It returns the outcome of the dispatcher or prints exception message.
    """
    try:
        input_str = test_input_str or input("> ")
        if _is_input_valid(input_str):
            command, kv_args = parse_input(input_str)
            return validate_and_dispatch_command(command, kv_args, txn)
    except KeyNotFoundError as e:
        print(e, file=sys.stderr)
        return e.message
    except ValueError as e:
        print(e, file=sys.stderr)
    except RuntimeError as e:
        print(e, file=sys.stderr)
        sys.exit(1)
    except (EOFError, KeyboardInterrupt):
        print(ExitOutcome.MESSAGE.value)
        sys.exit(0)
    return False
