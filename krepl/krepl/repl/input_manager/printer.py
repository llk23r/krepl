""" Contains method to print or raise an error. """
from typing import Optional

from krepl.krepl.db.database import KeyNotFoundError


def print_or_error(
    value: Optional[str], default_message: Optional[str] = None
) -> Optional[str]:
    """Prints a value or returns a default message if the value is None by raising a KeyNotFoundError."""
    if value is None and default_message is not None:
        raise KeyNotFoundError(default_message)
    elif value is not None and isinstance(value, str):
        print(value)
    return value
