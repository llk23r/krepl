""" Contains logic for the dispatcher. It is a function that takes the transaction block as an argument and returns a boolean. The dispatcher-handler returns a callable."""
from typing import List

from krepl.krepl.db.transactions import TransactionBlock
from krepl.krepl.repl.constants import Command, InactiveTransactionOutcome, ReadOutcome
from krepl.krepl.repl.input_manager.printer import print_or_error


def construct_command_map(args: List[str], txn: TransactionBlock) -> dict:
    """Constructs the command map.
    The command map is a dictionary that maps the command to the handler function.
    The handler function is a function that takes the transaction block as an argument and returns a boolean. The dispatcher-handler returns a callable."""
    key = args[0] if args else None
    value = args[1] if len(args) > 1 else None
    return {
        Command.READ.value: {
            "valid_arg_count": 1,
            "dispatcher": lambda: print_or_error(
                txn.read(key),
                default_message=f"{ReadOutcome.ERROR.value}: {key}",
            ),
        },
        Command.COMMIT.value: {
            "valid_arg_count": 0,
            "dispatcher": lambda: print_or_error(
                txn.commit(),
                default_message=f"{InactiveTransactionOutcome.ERROR.value}",
            ),
        },
        Command.ABORT.value: {
            "valid_arg_count": 0,
            "dispatcher": lambda: print_or_error(
                txn.abort(),
                default_message=f"{InactiveTransactionOutcome.ERROR.value}",
            ),
        },
        Command.WRITE.value: {
            "valid_arg_count": 2,
            "dispatcher": lambda: txn.write(key, value),
        },
        Command.DELETE.value: {
            "valid_arg_count": 1,
            "dispatcher": lambda: txn.delete(key),
        },
        Command.START.value: {
            "valid_arg_count": 0,
            "dispatcher": lambda: txn.start(),
        },
        Command.QUIT.value: {
            "valid_arg_count": 0,
            "dispatcher": lambda: txn.quit(),
        },
    }
