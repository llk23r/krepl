import enum


class Outcome(enum.Enum):
    """Enum for the possible outcomes of a REPL session."""

    pass


class ReadOutcome(Outcome):
    """Enum for the possible outcomes of a read command."""

    ERROR = "Key not found"


class InactiveTransactionOutcome(Outcome):
    """Enum for the possible outcomes of an inactive transaction."""

    ERROR = "No active transaction..."


class ExitOutcome(Outcome):
    """Enum for the possible outcomes of an exit command."""

    MESSAGE = "\nExiting..."


class InvalidInputOutcome(Outcome):
    """Enum for the possible outcomes of an invalid input."""

    ERROR = "Invalid input. Only ASCII characters allowed."


class LargeInputOutcome(Outcome):
    """Enum for the possible outcomes of a large input."""

    ERROR = "Input too large. Max size"


class LogPathNotSetOutcome(Outcome):
    """Enum for the possible outcomes of a log path not set."""

    WARNING = "LOG_PATH is not set in config.py. Logging will not be saved."


class PythonPathNotSetOutcome(Outcome):
    """Enum for the possible outcomes of a python path not set."""

    INFO = 'Check if $PYTHON_PATH is set using <echo $PYTHONPATH>.\nIf not set it with: <export PYTHONPATH="${PYTHONPATH}:${PWD}">'


class Command(enum.Enum):
    """Enum for the possible commands."""

    READ = "READ"
    WRITE = "WRITE"
    DELETE = "DELETE"
    START = "START"
    COMMIT = "COMMIT"
    ABORT = "ABORT"
    QUIT = "QUIT"
